package com.happycode.controller;

import com.happycode.bean.User;
import com.happycode.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class UserAction extends ActionSupport{
	
	
	// 接收用户传的参数
	private User user;
	
	// 回显数据
	private String info;
	
	UserService userService = new UserService();
	/*
	 * 模拟接收servlet层请求的数据
	 */
	public String login() {
		User dbUser = userService.login(user);
		
		info = dbUser != null? "登录成功":"登录失败!帐号或密码错误！";
		return  dbUser != null? "success":"error";
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
