package com.happycode.service;

import com.happycode.bean.User;
import com.happycode.dao.UserDao;

public class UserService {
	
	UserDao userDao = new UserDao();
	
	public User login(User user) {
		User dbUser = userDao.getUserByCheck(user);
		return dbUser;
	}
	
	public boolean register(User user) {
		int uid = userDao.register(user);
		return uid > 0 ? true:false;
	}
}
