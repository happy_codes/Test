package com.happycode.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.happycode.bean.User;

public class UserDao {

	private static String cdburl = "jdbc:mysql://localhost:3306/mydb01";
	private static String cdbuser = "root";
	private static String cdbpassword = "twh123456";
	private static String cdbdriver = "com.mysql.jdbc.Driver";
	
	public User getUserByCheck(User user) {
		User dbUser = null;
		try {
			// 2、加载驱动
			Class.forName(cdbdriver);
			
			// 3、获得连接
			Connection con = DriverManager.getConnection(cdburl, cdbuser, cdbpassword);
			
			// 4、查找用户
			String sql = "select id, username, password, age, sex from users "
					+ "where username = ? and password = ?";
			
			PreparedStatement pstmt = con.prepareStatement(sql);
			
			System.out.println(user.getUserName() +"\n"+ user.getPassword());
			pstmt.setString(1, user.getUserName());
			pstmt.setString(2, user.getPassword());
			
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.next()) {
				dbUser = new User();
				dbUser.setUid(rs.getInt("id"));
				dbUser.setUserName(rs.getString("username"));
				dbUser.setPassword(rs.getString("password"));
				dbUser.setAge(rs.getInt("age"));
				dbUser.setSex(rs.getInt("sex"));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return dbUser;
	}
	
	public int register(User user) {
		int uid = 0;
		/*
		try {
			// 2、加载驱动
			Class.forName(dbdriver);
			
			// 3、获得连接
			Connection con = DriverManager.getConnection(dburl, dbuser, dbpassword);
			
			// 4、往数据中users表插入一条记录
			String sql = "insert into users (username, password, age, sex) values(?, ?, ?, ?)";
			
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, user.getUserName());
			pstmt.setString(2, user.getPassword());
			pstmt.setInt(3, user.getAge());
			pstmt.setInt(4, user.getSex());
			
			uid = pstmt.executeUpdate();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}*/
		return uid;
	}
}
