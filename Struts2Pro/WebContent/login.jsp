<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>登录页面</title>

    <!-- Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  
  <body>
	<div class="containere">

      <div class="col-md-12">
        <h1>登录</h1>
      </div>
      
      <div class="row">
	    <div class="col-md-2"></div>
		  
	  <div class="col-md-4" >
	    <form action="login">
		  <div class="form-group">
		    <label for="exampleInputEmail1">用户名</label>
		    <input name="user.userName" type="text" class="form-control" placeholder="UserName">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">密码</label>
		    <input name="user.password" type="password" class="form-control" placeholder="Password">
		  </div>
		  <button type="submit" class="btn btn-default">登录</button> ${info}
	    </form>
	  </div>
	  </div>
	</div>
  </body>
</html>
